<x-dashboard :title="langAction('edit', __('skeleton::package.singular'))">
    <x-heading class="flex justify-between items-center" :label="langAction('edit', __('skeleton::package.singular'))">
        <a href="{{ route('dashboard.bones.index') }}">
            <em class="far fa-reply mr-2"></em> @lang('rapture::actions.return')
        </a>
    </x-heading>

    <x-container>
        <x-form method="put" action="{{ route('dashboard.bones.update', $bone) }}">
            <div class="space-y-8">
                <x-form-section :title="__('skeleton::package.singular')">
                    <x-input name="name" :value="$bone->name" :label="__('rapture::field.name')" required />
                </x-form-section>

                <div class="text-right">
                    <x-button type="submit" size="large" color="primary">
                        @lang('rapture::actions.save')
                    </x-button>
                </div>
            </div>
        </x-form>
    </x-container>
</x-dashboard>
