<x-dashboard :title="__('skeleton::package.plural')">
    <x-heading class="flex justify-between items-center" :label="__('skeleton::package.plural')">
        @can('bones.create', 'bones')
            <x-quick-create :action="route('dashboard.bones.store')" icon="plus" :label="langAction('new', __('skeleton::package.singular'))" />
        @endcan
    </x-heading>

    <livewire:bone-table />
</x-dashboard>
