<?php

Route::middleware(['web', 'auth'])
    ->namespace('Rapture\Skeletons\Controllers')
    ->prefix('dashboard')
    ->name('dashboard.')
    ->group(function () {
        Route::resource('skeletons', 'BonesController')->except('create', 'show', 'destroy');
    });
