<?php

namespace Rapture\Skeletons\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Rapture\Hooks\Facades\Hook;
use Rapture\Skeletons\Events\BoneCreated;
use Rapture\Skeletons\Events\BoneUpdated;
use Rapture\Skeletons\Models\Bone;
use Rapture\Skeletons\Requests\StoreBone;
use Rapture\Skeletons\Requests\UpdateBone;

class BonesController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Bone::class, 'bone');
    }

    public function index()
    {
        return view('skeleton::dashboard.index');
    }

    public function store(StoreBone $request)
    {
        $bone = Bone::create([
            'name' => $request->input('name'),
        ]);

        Hook::dispatch('bone.stored', new BoneCreated($bone));

        return redirect()->route('dashboard.bones.edit', $bone->id);
    }

    public function edit(Bone $bone)
    {
        return view('skeleton::dashboard.edit', [
            'bone' => $bone,
        ]);
    }

    public function update(UpdateBone $request, Bone $bone)
    {
        $bone->name = $request->input('name');
        $bone->save();

        Hook::dispatch('bone.updated', new BoneUpdated($bone));

        return redirect()
            ->route('dashboard.bones.index')
            ->with('status', langAlert('updated', __('skeleton::package.singular')));
    }
}
