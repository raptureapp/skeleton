<?php

namespace Rapture\Skeletons\Events;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Rapture\Skeletons\Models\Bone;

class BoneCreated
{
    use Dispatchable, SerializesModels;

    public $bone;

    public function __construct(Bone $bone)
    {
        $this->bone = $bone;
    }
}
