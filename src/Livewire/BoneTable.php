<?php

namespace Rapture\Skeletons\Livewire;

use Rapture\Core\Columns\Date;
use Rapture\Core\Columns\ID;
use Rapture\Core\Columns\Text;
use Rapture\Core\Livewire\DatatableComponent;
use Rapture\Core\Table\Action;
use Rapture\Hooks\Facades\Hook;
use Rapture\Skeletons\Events\BoneDeleted;
use Rapture\Skeletons\Models\Bone;

class BoneTable extends DatatableComponent
{
    public $table = 'dashboard.bones';

    public function columns()
    {
        return [
            ID::make(),
            Text::make('name', __('rapture::field.name'))
                ->visible(),
            Date::make('created_at', __('rapture::field.created'))
                ->defaultSort(),
            Date::make('updated_at', __('rapture::field.updated')),
        ];
    }

    public function actions()
    {
        return [
            Action::edit('bones')->primary(),
            Action::delete('bones'),
        ];
    }

    public function delete(Bone $bone)
    {
        $this->authorize('bones.destroy', $bone);

        $bone->delete();

        Hook::dispatch('bone.deleted', new BoneDeleted($bone));
    }

    public function query()
    {
        return Bone::query();
    }
}
