<?php

namespace Rapture\Skeletons\Models;

use Illuminate\Database\Eloquent\Model;

class Bone extends Model
{
    protected $guarded = [];
}
