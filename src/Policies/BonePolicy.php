<?php

namespace Rapture\Skeletons\Policies;

use Rapture\Keeper\Policies\BasePolicy;

class BonePolicy extends BasePolicy
{
    protected $key = 'bones';
}
