<?php

namespace Rapture\Skeletons\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Rapture\Hooks\Facades\Filter;

class UpdateBone extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('bones.edit');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Filter::dispatch('bones.update.validation', [
            'name' => 'required|max:255',
        ]);
    }
}
