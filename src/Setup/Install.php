<?php

namespace Rapture\Skeletons\Setup;

use Rapture\Packages\BaseInstall;

class Install extends BaseInstall
{
    public function handle()
    {
        $this->resourcePermissions('bones');

        $this->addMenu([
            'label' => 'skeleton::package.plural',
            'route' => 'dashboard.bones.index',
            'icon' => 'skull',
            'namespaces' => ['dashboard/bones'],
            'permission' => 'bones.index',
        ]);
    }
}
