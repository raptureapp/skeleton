<?php

namespace Rapture\Skeletons;

use Illuminate\Support\Facades\Gate;
use Rapture\Packages\Package;
use Rapture\Packages\Providers\PackageProvider;
use Rapture\Skeletons\Livewire\BoneTable;
use Rapture\Skeletons\Models\Bone;
use Rapture\Skeletons\Policies\BonePolicy;

class SkeletonServiceProvider extends PackageProvider
{
    public function configure(Package $package)
    {
        $package->name('skeleton')
            ->translations()
            ->migrations()
            ->routes('web')
            ->views()
            ->component('bone-table', BoneTable::class)
            ->installer();
    }

    public function installed()
    {
        Gate::policy(Bone::class, BonePolicy::class);
    }
}
